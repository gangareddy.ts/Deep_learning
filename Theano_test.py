__author__ = 'ingared'

import theano
import theano.tensor as T
from theano import function
from theano import pp

x = T.dscalar('x')
y = T.dscalar('y')
z = x + y
f = function([x,y],z)
print pp(z)

f(2,3)
z.eval({x:2,y:3})
# d scalar is a double scalar of 0- dimensional arrays.
# f is being compiled to c code

# pp -s pretty print of function

# For function on matrices change the type of x and y as

x1 = T.dmatrix('x1')
y1 = T.dmatrix('y1')
z1 = x1 + y1
f1 = function([x1,y1],z1)

print pp(z1)


'''

    byte: bscalar, bvector, bmatrix, brow, bcol, btensor3, btensor4
    16-bit integers: wscalar, wvector, wmatrix, wrow, wcol, wtensor3, wtensor4
    32-bit integers: iscalar, ivector, imatrix, irow, icol, itensor3, itensor4
    64-bit integers: lscalar, lvector, lmatrix, lrow, lcol, ltensor3, ltensor4
    float: fscalar, fvector, fmatrix, frow, fcol, ftensor3, ftensor4
    double: dscalar, dvector, dmatrix, drow, dcol, dtensor3, dtensor4
    complex: cscalar, cvector, cmatrix, crow, ccol, ctensor3, ctensor4

'''

a = theano.tensor.vector()
out = a + a**10
f2 = theano.function([a],out)
print f2([0,1,2])


## Multiple arguments

a, b = T.dmatrices('a','b')
diff = a - b
abs_diff = abs(diff)
diff_squared = diff**2
f = function([a,b],[diff,abs_diff,diff_squared])

print f([[1,1],[1,1]],[[0,1],[2,3]])


## Default value for an argument
from theano import Param
f = function([x,Param(y,default = 21)],z)
f(33)

## Shared variables
from theano import shared
state = shared(0)
inc = T.iscalar('inc')
T.iscalar('inc')
accumulator = function([inc],state, updates=[(state, state+inc)])
decrementor = function([inc],state, updates=[(state, state-inc)])


## using Random numbers

from theano.tensor.shared_randomstreams import RandomStreams

srng = RandomStreams(seed= 234)