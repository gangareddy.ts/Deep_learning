__author__ = 'ingared'

"""
This tutorial introduces the multilayer perceptron using Theano.

 A multilayer perceptron is a logistic regressor where
instead of feeding the input to the logistic regression you insert a
intermediate layer, called the hidden layer, that has a nonlinear
activation function (usually tanh or sigmoid) . One can use many such
hidden layers making the architecture deep. The tutorial will also tackle
the problem of MNIST digit classification.

.. math::

    f(x) = G( b^{(2)} + W^{(2)}( s( b^{(1)} + W^{(1)} x))),

References:

    - textbooks: "Pattern Recognition and Machine Learning" -
                 Christopher M. Bishop, section 5

"""
__docformat__ = 'restructedtext en'


# Import
from General import *
from ML_classes import LogisticRegression

class HiddenLayer(object):
    def __init__(self, rng, input, n_in, n_out, W=None, b=None, activation=T.tanh):

        """
        Typical hidden layer of a MLP: units are fully-connected and have
        sigmoidal activation function. Weight matrix W is of shape (n_in,n_out)
        and the bias vector b is of shape (n_out,).

        NOTE : The non linearity used here is tanh

        Hidden unit activation is given by: tanh(dot(input,W) + b) instead of sigmoid

        :type rng: numpy.random.RandomState
        :param rng: a random number generator used to initialize weights

        :type input: theano.tensor.dmatrix
        :param input: a symbolic tensor of shape (n_examples, n_in)

        :type n_in: int
        :param n_in: dimensionality of input

        :type n_out: int
        :param n_out: number of hidden units

        :type activation: theano.output function
        :param activation: Non linearity to be applied in the hidden layer

        """

        self.input = input

        # `W` is initialized with `W_values` which is uniformely sampled
        # from sqrt(-6./(n_in+n_hidden)) and sqrt(6./(n_in+n_hidden))
        # for tanh activation function
        #
        # the output of uniform if converted using asarray to dtype
        # theano.config.floatX so that the code is runable on GPU
        # Note : optimal initialization of weights is dependent on the
        #        activation function used (among other things).
        #        For example, results presented in [Xavier10] suggest that you
        #        should use 4 times larger initial weights for sigmoid
        #        compared to tanh
        #        We have no info for other function, so we use the same as
        #        tanh.

        min_limit = -1.0*numpy.sqrt(4./(n_out + n_in))
        max_limit = numpy.sqrt(4./(n_out + n_in))
        if W is None:

            W_values = numpy.asarray(rng.uniform(low= min_limit, high= max_limit,size=(n_in,n_out)), dtype = theano.config.floatX)

            if activation == theano.tensor.nnet.sigmoid:
                W_values *= 4

            W = theano.shared(value= W_values,name= 'W',borrow = True)

        if b is None:

            b_values = numpy.zeros((n_out,),dtype= theano.config.floatX)
            b = theano.shared(value=b_values, name='b',borrow = True )

        self.W = W
        self.b = b

        self.params = [self.W, self.b]

        lin_output = T.dot(input,self.W) + self.b
        if activation is None:
            self.output = lin_output
        else:
            self.output = activation(lin_output)


class MLP(object):

    """
    Multi-Layer Perceptron Class

    A multilayer perceptron is a feed forward artificial neural network model
    that has one layer or more of hidden units and nonlinear activations.
    Intermediate layers usually have as activation function tanh or the
    sigmoid function (defined here by a ``HiddenLayer`` class)  while the
    top layer is a softamx layer (defined here by a ``LogisticRegression``
    class).

    """

    def __init__(self, rng, input, n_in, n_hidden, n_out):

        """
        Initialize the parameters for the multilayer perceptron

        :type rng: numpy.random.RandomState
        :param rng: a random number generator used to initialize weights

        :type input: theano.tensor.TensorType
        :param input: symbolic variable that describes the input of the
        architecture (one minibatch)

        :type n_in: int
        :param n_in: number of input units, the dimension of the space in
        which the datapoints lie

        :type n_hidden: int
        :param n_hidden: number of hidden units

        :type n_out: int
        :param n_out: number of output units, the dimension of the space in
        which the labels lie

        """

        # Since we are dealing with a one hidden layer MLP, this will translate
        # into a HiddenLayer with a tanh activation function connected to the
        # LogisticRegression layer; the activation function can be replaced by
        # sigmoid or any other nonlinear function


        # Note : This MLP assumes only one hidden layer (a very simple case but not a generic one)

        self.hiddenLayer = HiddenLayer(rng = rng,input=input,n_in=n_in, n_out= n_hidden, activation= T.tanh)
        self.logRegressionLayer = LogisticRegression(input = self.hiddenLayer.output,n_in = n_hidden,n_out = n_out)

        # L1  regularization option is to enforce L1 norm to be small
        self.L1 = abs(self.hiddenLayer.W.sum()) + abs(self.logRegressionLayer.W.sum())

        # L2 one regularization option is to enforce square of L2 norm to be small
        self.L2_sqr = sum(self.logRegressionLayer.W**2) + sum(self.hiddenLayer.W**2)

        self.negative_log_likelihood = self.logRegressionLayer.negative_log_likelihood
        self.errors = self.logRegressionLayer.errors

        self.params = self.hiddenLayer.params + self.logRegressionLayer.params


def test_mlp(learning_rate=0.01, L1_reg=0.00, L2_reg=0.0001, n_epochs=1000, dataset='mnist.pkl.gz', batch_size=20, n_hidden=500):

    """

    Demonstrate stochastic gradient descent optimization for a multilayer
    perceptron

    This is demonstrated on MNIST.

    :type learning_rate: float
    :param learning_rate: learning rate used (factor for the stochastic
    gradient

    :type L1_reg: float
    :param L1_reg: L1-norm's weight when added to the cost (see
    regularization)

    :type L2_reg: float
    :param L2_reg: L2-norm's weight when added to the cost (see
    regularization)

    :type n_epochs: int
    :param n_epochs: maximal number of epochs to run the optimizer

    :type dataset: string
    :param dataset: the path of the MNIST dataset file from
                 http://www.iro.umontreal.ca/~lisa/deep/data/mnist/mnist.pkl.gz


   """
    datasets = load_data(dataset)

    train_set_x, train_set_y = datasets[0]
    valid_set_x, valid_set_y = datasets[1]
    test_set_x, test_set_y = datasets[2]

    # compute number of mini batches for training, validation and testing
    n_train_batches = train_set_x.get_value(borrow=True).shape[0] / batch_size
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0] / batch_size
    n_test_batches = test_set_x.get_value(borrow=True).shape[0] / batch_size

    # Build the model
    print "... Building the model"

    rng = numpy.random.RandomState(1234)

    # Build the params
    index = T.lscalar()
    x = T.matrix(name='x')
    y = T.ivector(name='y')

    classifier = MLP(rng,input=x,n_in= 28*28,n_hidden= n_hidden ,n_out=10)

    cost = (classifier.negative_log_likelihood(y) + L1_reg*classifier.L1 + L2_reg* classifier.L2_sqr)

    gradients = [T.grad(cost,wrt=param) for param in classifier.params]
    updates = [(param, param - learning_rate*grad_param) for param, grad_param in zip(classifier.params,gradients)]

    train_model = theano.function(inputs=[index], outputs=[cost],
                                  updates = updates,
                                  givens=
                                  {
                                    x: train_set_x[index*n_train_batches :(index+1)*n_train_batches],
                                    y: train_set_y[index*n_train_batches : (index+1)*n_train_batches]
                                  })

    test_model = theano.function(inputs=[index], outputs= classifier.errors(y),
                                 givens =
                                 {
                                  x: test_set_x[index*n_test_batches : (index+1)*n_test_batches],
                                  y: test_set_y[index*n_test_batches : (index+1)*n_test_batches]
                                 })

    validation_model = theano.function(inputs=[index], outputs= classifier.errors(y),
                                       givens=
                                       {
                                           x: valid_set_x[index*n_test_batches : (index+1)*n_test_batches],
                                           y: valid_set_y[index*n_test_batches : (index+1)*n_test_batches]
                                       }
                                        )

    #################
    # TRAINING MODEL
    #################
    print '... training'

    # early-stopping parameters
    patience = 10000  # look as this many examples regardless
    patience_increase = 2  # wait this much longer when a new best is found
    improvement_threshold = 0.995  # a relative improvement of this much is considered significant
    validation_frequency = min(n_train_batches, patience / 2)
                                  # go through this many
                                  # minibatche before checking the network
                                  # on the validation set; in this case we
                                  # check every epoch

    best_validation_loss = numpy.inf
    best_iter = 0
    test_score = 0.
    start_time = time.clock()

    epoch = 0
    done_looping = False

    while ( (epoch < n_epochs) and ( not done_looping) ):

        epoch = epoch + 1
        for minibatch_index in xrange(n_train_batches):

            minibatch_avg_cost = train_model(minibatch_index)

            # iteration number
            iter = (epoch-1)*n_train_batches + minibatch_index

            if ((iter + 1) % validation_frequency) == 0:
                # computing zero one loss in validation set

                validation_losses = [validation_model(i) for i in xrange(n_valid_batches)]
                validation_loss = numpy.mean(validation_losses)

                print((' epoch %i, minibatch %i/%i, validation error  %f %%') %
                          (epoch, minibatch_index + 1, n_train_batches, validation_loss * 100.))

                if validation_loss < best_validation_loss:
                    if validation_loss < best_validation_loss*improvement_threshold:
                        patience = max(patience, patience_increase*iter)

                    best_validation_loss = validation_loss
                    best_iter = iter

                    # test it on the test set
                    test_losses = [test_model(i) for i in xrange(n_test_batches)]
                    test_score = numpy.mean(test_losses)

                    print((' epoch %i, minibatch %i/%i, test error of best model  %f %%') %
                          (epoch, minibatch_index + 1, n_train_batches, test_score * 100.))


            if patience <= iter:
                done_looping = True
                break


    end_time = time.clock()
    print(('Optimization complete. Best validation score of %f %% obtained at iteration %i, with test performance %f %%') %
          (best_validation_loss * 100., best_iter + 1, test_score * 100.))

    print >> sys.stderr, ('The code for file ' + os.path.split(__file__)[1] + ' ran for %.2fm' % ((end_time - start_time) / 60.))


if __name__ == '__main__':
    test_mlp(dataset= 'data/mnist.pkl.gz')
