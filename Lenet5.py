__author__ = 'ingared'

from General import *
import theano
from ML_classes import LogisticRegression
from Neural_net import HiddenLayer

class LeNetConvPoolLayer(object):
    """Pool Layer of a convolutional network """

    def __init__(self, rng, input, filter_shape, image_shape, poolsize=(2, 2)):

        """
        Allocate a LeNetConvPoolLayer with shared variable internal parameters.

        :type rng: numpy.random.RandomState
        :param rng: a random number generator used to initialize weights

        :type input: theano.tensor.dtensor4
        :param input: symbolic image tensor, of shape image_shape

        :type filter_shape: tuple or list of length 4
        :param filter_shape: (number of filters, num input feature maps,
                              filter height, filter width)

        :type image_shape: tuple or list of length 4
        :param image_shape: (batch size, num input feature maps,
                             image height, image width)

        :type poolsize: tuple or list of length 2
        :param poolsize: the downsampling (pooling) factor (#rows, #cols)
        """

        assert image_shape[1] == filter_shape[1]

        self.input = input

        # filter_shape =  no_of features * num input feature maps * filter height * filter width

        # inputs to each hidden unit
        fan_in = numpy.prod(filter_shape[1:])
        fan_out = (filter_shape[0] * numpy.prod(filter_shape[2:]) / numpy.prod(poolsize))

        # initialize weights with random weights
        W_bound = numpy.sqrt(6. / (fan_in + fan_out))

        self.W = theano.shared(
            numpy.asarray(
                rng.uniform(low=-W_bound, high=W_bound, size=filter_shape),
                dtype=theano.config.floatX
            ),
            borrow=True
        )

        # the bias is a 1D tensor -- one bias per output feature map
        b_values = numpy.zeros((filter_shape[0],), dtype=theano.config.floatX)
        self.b = theano.shared(value=b_values, borrow=True)

        self.params = [self.W , self.b]

        conv_out = conv.conv2d(input= input,filter_shape= filter_shape,filters= self.W, image_shape= image_shape)
        pooling_out = downsample.max_pool_2d(input= conv_out,ds= poolsize , ignore_border= True)

        self.output = T.tanh(pooling_out + self.b.dimshuffle('x',0,'x','x'))


class Implementation_Lenet5():

    def run(self,im_height,im_width,filter_height,filter_width, cp_layers, nkerns,
                          poolsize,batch_size=500,dataset='data/mnist.pkl.gz',learning_rate=0.13, n_epochs=1000):


        """

        :param im_height: height of image
        :param im_width: width of image

        Image of ( x,y) -- height = x, width = y

        :param filter_height:  height of filter
        :param filter_width: width of filter

        :param cp_layers: No of cp_layers

        :param nkerns: array showing no of feature maps in each layer

        :param batch_size: batch size of the data in a dataset

        :param poolsize: poolsize (2,2)

        :return:
        """

        x = T.matrix(name='x')
        y = T.ivector(name='y')
        index = T.iscalar(name='index')
        self.learning_rate = learning_rate

        print "Loading the data"

        datasets = load_data(dataset)

        train_set_x, train_set_y = datasets[0]
        valid_set_x, valid_set_y = datasets[1]
        test_set_x, test_set_y = datasets[2]

        # compute number of mini batches for training, validation and testing
        n_train_batches = train_set_x.get_value(borrow=True).shape[0] / batch_size
        n_valid_batches = valid_set_x.get_value(borrow=True).shape[0] / batch_size
        n_test_batches = test_set_x.get_value(borrow=True).shape[0] / batch_size

        print " ... data has been completely loaded"
        # Build the model
        print "... Building the model"

        rng = numpy.random.RandomState(1234)

        # First layer -- A Convolution layer + Max pooling layer

        image_shape = (batch_size,1,im_height,im_width)
        input1 = x.reshape(image_shape)
        maps = nkerns[0]
        filter_shape = (maps,image_shape[1],filter_height,filter_width)

        layer0 = LeNetConvPoolLayer(input= input1,image_shape= image_shape, filter_shape= filter_shape, poolsize= poolsize, rng= rng)
        count = 1
        layer = layer0
        self.params = []
        self.params += layer0.params

        # Loop for the next convolution layers

        while count < cp_layers:

            im_height = (im_height - filter_height + 1)/(poolsize[0])
            im_width = ( im_width - filter_width + 1)/(poolsize[1])

            print image_shape, count-1
            image_shape = (batch_size,nkerns[count-1],im_height,im_width)
            input1 = layer.output
            print numpy.shape(input1), " is shape of output at layer " , count
            filter_shape = (nkerns[count],image_shape[1],filter_height,filter_width)
            next_layer = LeNetConvPoolLayer(input= input1,image_shape= image_shape, filter_shape= filter_shape, poolsize= poolsize, rng= rng)
            self.params += next_layer.params
            layer = next_layer
            count += 1

        # Last but layer is a fully connected hidden layer
        input1 = layer.output.flatten(2)
        n_in = nkerns[-1]*(im_height - filter_height + 1)*( im_width - filter_width + 1)/(poolsize[0]*poolsize[1])
        n_out = batch_size

        hl = HiddenLayer(rng = rng,input = input1,n_in= n_in , n_out = n_out, activation= T.tanh)
        self.params += hl.params

        ll = LogisticRegression(input = hl.output, n_in = batch_size, n_out = 10 )
        self.params += ll.params

        cost = ll.negative_log_likelihood(y)

        # Test
        test_model = theano.function([index],
                               ll.errors(y),
                               givens=
                               {
                                   x:test_set_x[index*batch_size: (index+1)*batch_size],
                                   y:test_set_y[index*batch_size: (index+1)*batch_size]
                               })
        validation_model = theano.function([index],
                               ll.errors(y),
                               givens=
                               {
                                   x:valid_set_x[index*batch_size: (index+1)*batch_size],
                                   y:valid_set_y[index*batch_size: (index+1)*batch_size]
                               })

        grad = T.grad(cost= cost, wrt= self.params)

        updates = [(params, params - self.learning_rate*grad_params) for params,grad_params in zip(self.params, grad)]

        # Train the model
        train_model = theano.function([index],
                                cost,
                                updates= updates,
                                givens=
                                {
                                    x:train_set_x[index*batch_size: (index+1)*batch_size],
                                    y:train_set_y[index*batch_size: (index+1)*batch_size]
                                }
                                )

        print " ... Building the model"

        best_validation_loss = numpy.inf
        best_iter = 0
        test_score = 0.
        start_time = time.clock()

        print '... training'

        # early-stopping parameters
        patience = 10000  # look as this many examples regardless
        patience_increase = 2  # wait this much longer when a new best is found
        improvement_threshold = 0.995  # a relative improvement of this much is considered significant
        validation_frequency = min(n_train_batches, patience / 2)


        epoch = 0
        done_looping = False

        while ( (epoch < n_epochs) and ( not done_looping) ):

            epoch = epoch + 1
            for minibatch_index in xrange(n_train_batches):

                minibatch_avg_cost = train_model(minibatch_index)

                # iteration number
                iter = (epoch-1)*n_train_batches + minibatch_index

                if ((iter + 1) % validation_frequency) == 0:
                    # computing zero one loss in validation set

                    validation_losses = [validation_model(i) for i in xrange(n_valid_batches)]
                    validation_loss = numpy.mean(validation_losses)

                    print((' epoch %i, minibatch %i/%i, validation error  %f %%') %
                              (epoch, minibatch_index + 1, n_train_batches, validation_loss * 100.))

                    if validation_loss < best_validation_loss:
                        if validation_loss < best_validation_loss*improvement_threshold:
                            patience = max(patience, patience_increase*iter)

                        best_validation_loss = validation_loss
                        best_iter = iter

                        # test it on the test set
                        test_losses = [test_model(i) for i in xrange(n_test_batches)]
                        test_score = numpy.mean(test_losses)

                        print((' epoch %i, minibatch %i/%i, test error of best model  %f %%') %
                              (epoch, minibatch_index + 1, n_train_batches, test_score * 100.))


                if patience <= iter:
                    done_looping = True
                    break


        end_time = time.clock()
        print(('Optimization complete. Best validation score of %f %% obtained at iteration %i, with test performance %f %%') %
              (best_validation_loss * 100., best_iter + 1, test_score * 100.))

        print >> sys.stderr, ('The code for file ' + os.path.split(__file__)[1] + ' ran for %.2fm' % ((end_time - start_time) / 60.))

if __name__ == '__main__':
    a = Implementation_Lenet5()
    a.run(im_height=28,im_width= 28, filter_height= 5, filter_width= 5, poolsize=(2,2),cp_layers=2,nkerns=[2,4],dataset='data/mnist.pkl.gz')
